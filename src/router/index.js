const options = {
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/routeMap/version2',
        },
        {
            path: '/route_map',
            name: 'route_map',
            component: () => import('../components/routeMap')
        },
        {
            path: '/heat_map',
            name: 'heat_map',
            component: () => import('../components/heatMap')
        },
        {
            path: '/heat_map_r',
            name: 'heat_map',
            component: () => import('../components/heatMapR')
        },
        {
            path: '/routeMap/version1',
            name: 'routeMap_version1',
            component: () => import('../components/routeMapVersion1')
        },
        {
            path: '/routeMap/version2',
            name: 'routeMap_version2',
            component: () => import('../components/routeMapVersion2')
        },
    ]
}
export default options
