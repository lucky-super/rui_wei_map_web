import axios from 'axios'
// axios.defaults.timeout = 60000
axios.defaults.withCredentials= true
// http method
const METHOD = {
    GET: 'get',
    POST: 'post'
}
async function request(url, method, params, config){
    switch (method) {
        case METHOD.GET:
            return axios.get(url, {params, ...config})
        case METHOD.POST:
            return axios.post(url, params, config)
        default:
            return axios.get(url, {params, ...config})
    }
}
export{
    request,
    METHOD
}
