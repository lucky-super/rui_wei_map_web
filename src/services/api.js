const BASE_URL = 'http://201ow31656.iok.la:43528'
module.exports = {
    MAP_INFORMATION: `${BASE_URL}/api/navigate/getFloor`,//楼层数据
    ERECT:`${BASE_URL}/api/navigate/getBuilding`,//建筑物数据
    INIT_MAP:`${BASE_URL}/api/navigate/init/`,//初始化数据
    HISTORY_MAP:`${BASE_URL}/api/navigate/history`,//路径图历史数据
    HEAT_MAP:`${BASE_URL}/api/navigate/heatMap`,//热力图
    MAP_POINTS:`${BASE_URL}/api/navigate/getPath`,//路网图数据
    SHOP_FLOW:`${BASE_URL}/api/navigate/getShopFlow`,//客流信息
    SHOP_INFO:`${BASE_URL}/api/navigate/getShopInfo/`,//店铺信息
}
