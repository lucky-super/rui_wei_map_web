import Vue from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue'
import moment from 'moment';
import 'moment/locale/zh-cn';
import 'ant-design-vue/dist/antd.css';
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const options = require('./router/index').default
const router = new VueRouter(options);
Vue.config.productionTip = false
moment.locale('zh-cn');
Vue.use(Antd)
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
